package cn.parsec.thrift.hello;

import java.util.Random;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import tutorial.*;

public class MultiplicationClient {
  public static void main(String [] args) {

   
    try {
      TTransport transport;
     
      transport = new TSocket("localhost", 9090);
      transport.open();

      TProtocol protocol = new  TBinaryProtocol(transport);
      MultiplicationService.Client client = new MultiplicationService.Client(protocol);

      perform(client);

      transport.close();
    } catch (TException x) {
      x.printStackTrace();
    } 
  }

  private static void perform(MultiplicationService.Client client) throws TException
  {
    int a,b,product;

    Random rand = new Random();
    long timeNow = System.currentTimeMillis();
    for(int i=0; i<10000; i++)
    {
        a=rand.nextInt(100);
        b=rand.nextInt(100);
//        System.out.print(a+"x"+b+"=");   
        product = client.multiply(a,b);
//        System.out.println(product);
    }
    System.out.println("Execution time="+(System.currentTimeMillis()-timeNow));
  }
}
